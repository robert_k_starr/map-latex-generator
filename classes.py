def createTable(key_array, dictionary):
    table_tex = "\t\\begin{tabular}{|l|l|}\n"
    table_tex = table_tex + "\t\t\\hline\n"

    for value in key_array:
        table_tex = table_tex + value
        table_tex = table_tex + "\t&\t"
        table_tex = table_tex + dictionary[value] + "\t\\\\\n"

    table_tex = table_tex + "\t\t\\hline\n\t\\end{tabular}\n"
    return table_tex


def encloseTable(table_tex):
    return "\\begin{table}[h!]\n\t\\centering\n\t\\smaller\n" + table_tex + "\\end{table}\n"


def cleanCoordinates(coordinates):
    return coordinates.replace("°","").replace("-","")


upslope_boundary_dictionary = {
    "G": "Glacial",
    "T": "Talus",
    "M": "Mixed",
    "U": "Unclear"
}


everything_else_dictionary = {
    "TP": "Terminates at Pond",
    "TNP": "Terminates Near Pond",
    "TL": "Terminates on Land",
    "MP": "Meltwater Present",
    "MN": "Meltwater (None)",
    "LC": "Clear",
    "LV": "Vague",
    "LN": "None",
    "TC": "Clear",
    "TV": "Vague",
    "TN": "None",
    "S": "Steep",
    "G": "Gentle",
    "U": "Unclear",
    "OC": "Clear",
    "OV": "Vague",
    "SS": "Snow",
    "SP": "Partial Snow",
    "SN": "None"
}

def getRange(initials):
    if (initials == "GV"):
        return "Gros Ventre"
    if (initials == "WR"):
        return "Wind River"
    if (initials == "T"):
        return "Teton"

class Map:
    def __init__(self):
        self.uniqueID = 0
        self.source = ""
        self.all_values = {}

    def caption(self):
        text = "Rock glacier " + str(self.uniqueID) + "."
        text = text + " Blue outline indicates perimeter. "
        text = text + "Image via " + str(self.source) + "."
        text = text + " Map by author."

        return "\\caption{" + text + "}"

    def setID(self, id):
        self.uniqueID = id

    def setSource(self, source):
        if (source == "LANDSAT"):
            self.source = "Landsat"
        else:
            self.source = source

    def setAllValues(self, dictionary):
        self.all_values = dictionary

    def produceOutput(self):
        figure_tex = self.outputFigureTex()
        table1 = self.outputTable1()
        table2 = self.outputTable2()
        return figure_tex + encloseTable(table1 + table2) + "\\clearpage\n"

    def graphicTex(self):
        final_tex = "\\includegraphics[width=.9\\linewidth]{Figs/" + str(self.uniqueID) + "}"
        # final_tex = "\\includegraphics[width=.9\\linewidth]{Figs/01}"
        return final_tex

    def labelTex(self):
        final_tex = "\\label{fig:" + str(self.uniqueID) + "}"
        return final_tex

    def outputFigureTex(self):
        final_tex = "\\begin{figure}[h!]\n\t\\centering\n"
        final_tex = final_tex + "\t" + self.graphicTex() + "\n"
        final_tex = final_tex + "\t" + self.caption() + "\n"
        final_tex = final_tex + "\t" + self.labelTex() + "\n"
        final_tex = final_tex + "\\end{figure}\n"
        return final_tex

    def date(self):
        date_text = str(self.all_values["YYYY"]) + "\_"
        date_text = date_text + str(self.all_values["MM"]) + "\_"
        date_text = date_text + str(self.all_values["DD"])
        return date_text

    def coordinates(self):
        coordinate_text = cleanCoordinates(self.all_values["N"]) + "$^\circ$ N, "
        coordinate_text = coordinate_text + cleanCoordinates(self.all_values["W"]) + "$^\circ$ W "
        return coordinate_text

    def outputTable1(self):
        key_array = ["RANGE", "DATE", "COORDINATES"]
        table_dictionary = {"RANGE": getRange(self.all_values["RANGE"]), "DATE": self.date(), "COORDINATES": self.coordinates()}

        return createTable(key_array, table_dictionary)

    def outputTable2(self):
        key_array = [
            "ID",
            "UPSLOPE",
            "TERMINUS LOCATION",
            "OUTLINE",
            "MELTWATER",
            "LONGITUDINAL FLOW",
            "TRANSVERSE FLOW",
            "ANGLE OF REPOSE",
            "SNOW COVER",
            "ASPECT",
            "UPVALLEY ELEVATION (m)",
            "DOWNVALLEY ELEVATION (m)",
            "AVERAGE ELEVATION (m)",
            "$^\circ$ SLOPE"
        ]
        table_dictionary = {
            "RG/GL/O": self.all_values["RG/GL/O"],
            "ID": str(self.uniqueID),
            "UPSLOPE": upslope_boundary_dictionary[self.all_values["UPSLOPE BOUNDARY"].replace(" ", "")],
            "TERMINUS LOCATION": everything_else_dictionary[self.all_values["TERMINUS LOCATION"].replace(" ", "")],
            "OUTLINE": everything_else_dictionary[self.all_values["OUTLINE"].replace(" ", "")],
            "MELTWATER": everything_else_dictionary[self.all_values["MELT WATER"].replace(" ", "")],
            "LONGITUDINAL FLOW": everything_else_dictionary[self.all_values["LONG FLOW"].replace(" ", "")],
            "TRANSVERSE FLOW": everything_else_dictionary[self.all_values["TRANS. FLOW"].replace(" ", "")],
            "ANGLE OF REPOSE": everything_else_dictionary[self.all_values["ANGLE OF REPOSE"].replace(" ", "")],
            "SNOW COVER": everything_else_dictionary[self.all_values["SNOW COVER"].replace(" ", "")],
            "ASPECT": self.all_values["ASPECT"],
            "UPVALLEY ELEVATION (m)": self.all_values["UPVALLEY ELEV."],
            "DOWNVALLEY ELEVATION (m)": self.all_values["DOWNVALLEY ELEV."],
            "AVERAGE ELEVATION (m)": self.all_values["AEL"],
            "$^\circ$ SLOPE": self.all_values["°SLOPE"]
        }
        return createTable(key_array, table_dictionary)