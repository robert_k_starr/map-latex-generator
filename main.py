## Import data
##	Assign each to a class
##	Convert class to text
##	Export text

from variables import *
from functions import *

output_file = open("output.tex", "w")
output_file.write(processTSV(TSVFile1) + processTSV(TSVFile2) + processTSV(TSVFile3))