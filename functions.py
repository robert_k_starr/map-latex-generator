from classes import *

def processTSV(tsvFileName):
    file = open(tsvFileName, 'r')
    key = file.readline().split("\t")
    maps = []
    for line in file:
        maps.append(createMapFromLine(key, line.split("\t")))
    output_text = ""
    for map in maps:
        output_text = output_text + map.produceOutput()
    return output_text


def clean(line):
    return line.replace("\n","")


def createMapFromLine(key, entries):
    dictionary = {}
    for value in range(0, len(key)):
        dictionary[clean(key[value])] = clean(entries[value])
    return_map = Map()
    return_map.setID(dictionary["ID"])
    return_map.setSource(dictionary["IMAGE SOURCE"])
    return_map.setAllValues(dictionary)

    return return_map
